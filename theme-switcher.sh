
#! /bin/bash

#read -p "theme name:" THEME

THEME=$1

# awesomewm
AWESOME_THEME_PATH="~/.config/awesome/themes/$THEME/theme.lua"
sed -i "/awesome\/themes/c\beautiful.init('$AWESOME_THEME_PATH')" /home/$USER/.config/awesome/rc.lua

# rofi
sed -i "/@theme/c\@theme \"./$THEME.rasi\"" /home/$USER/.config/rofi/config.rasi

# kitty
sed -i "/include/c\include ./$THEME.conf" /home/$USER/.config/kitty/kitty.conf

# betterlockscreen
rm "/home/$USER/.config/betterlockscreenrc"

# gtk
GTK_PATH="/home/$USER/.config/gtk-3.0/settings.ini"
GTK2_PATH="/home/$USER/.gtkrc-2.0"
# qt5ct
QT_PATH="/home/$USER/.config/qt5ct/qt5ct.conf"

if [ $THEME == "dracula" ]; then
  sed -i "/gtk-theme-name=/c\gtk-theme-name=Dracula" $GTK_PATH
  sed -i "/gtk-icon-theme=/c\gtk-icon-theme=Surfn-Dracul" $GTK_PATH
  sed -i "/gtk-icon-theme-name=/c\gtk-icon-theme-name=Surfn-Dracul" $GTK_PATH
  sed -i "/gtk-cursor-theme-name=/c\gtk-cursor-theme-name=Dracula-cursors" $GTK_PATH
  sed -i "/gtk-theme-name=/c\gtk-theme-name=\"Dracula\"" $GTK2_PATH
  sed -i "/gtk-icon-theme-name=/c\gtk-icon-theme-name=\"Surfn-Dracul\"" $GTK2_PATH
  sed -i "/gtk-cursor-theme-name=/c\gtk-cursor-theme-name=\"Dracula-cursors\"" $GTK2_PATH
  sed -i "/icon_theme=/c\icon_theme=Surfn-Dracul" $QT_PATH

  kvantummanager --set Dracula-purple-solid

  LOCKSCREEN_PATH="/home/$USER/.config/betterlockscreen/dracula-betterlockscreen"
  cp "$LOCKSCREEN_PATH/betterlockscreenrc" "/home/$USER/.config/"
  betterlockscreen -u "$LOCKSCREEN_PATH/lockscreen.jpg"

  GREETER_PATH="/home/$USER/.config/lightdm-mini-greeter/dracula-lightdm-mini-greeter/lightdm-mini-greeter.conf"

  sed -i "/USER_NAME_HERE/c\user = $USER" $GREETER_PATH

  pkexec bash -c "rm /etc/lightdm/lightdm-mini-greeter.conf;cp $GREETER_PATH /etc/lightdm/;systemctl restart lightdm.service"

elif [ $THEME == "nordic" ]; then
  sed -i "/gtk-theme-name=/c\gtk-theme-name=Nordic-darker" $GTK_PATH
  sed -i "/gtk-icon-theme=/c\gtk-icon-theme=Nordzy-dark" $GTK_PATH
  sed -i "/gtk-icon-theme-name=/c\gtk-icon-theme-name=Nordzy-dark" $GTK_PATH
  sed -i "/gtk-cursor-theme-name=/c\gtk-cursor-theme-name=Nordic-cursors" $GTK_PATH
  sed -i "/gtk-theme-name=/c\gtk-theme-name=\"Nordic-darker\"" $GTK2_PATH
  sed -i "/gtk-icon-theme-name=/c\gtk-icon-theme-name=\"Nordzy-dark\"" $GTK2_PATH
  sed -i "/gtk-cursor-theme-name=/c\gtk-cursor-theme-name=\"Nordic-cursors\"" $GTK2_PATH
  sed -i "/icon_theme=/c\icon_theme=Nordzy-dark" $QT_PATH

  kvantummanager --set Nordic-Darker-Solid

  LOCKSCREEN_PATH="/home/$USER/.config/betterlockscreen/nordic-betterlockscreen"
  cp "$LOCKSCREEN_PATH/betterlockscreenrc" "/home/$USER/.config/"
  betterlockscreen -u "$LOCKSCREEN_PATH/lockscreen.jpg"

  GREETER_PATH="/home/$USER/.config/lightdm-mini-greeter/nordic-lightdm-mini-greeter/lightdm-mini-greeter.conf"

  sed -i "/USER_NAME_HERE/c\user = $USER" $GREETER_PATH

  pkexec bash -c "rm /etc/lightdm/lightdm-mini-greeter.conf;cp $GREETER_PATH /etc/lightdm/;systemctl restart lightdm.service"

fi

# restart awesome
echo 'awesome.restart()' | awesome-client
